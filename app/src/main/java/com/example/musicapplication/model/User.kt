package com.example.musicapplication.model

data class User(
    var username: String? = "",
    var image: String? = "",
    val email: String? = ""
)
