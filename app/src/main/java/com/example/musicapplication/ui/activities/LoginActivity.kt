package com.example.musicapplication.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.commit
import com.example.musicapplication.R
import com.example.musicapplication.databinding.ActivityLoginBinding
import com.example.musicapplication.ui.fragments.LoginFragment

class LoginActivity : AppCompatActivity() {
    private val binding by lazy { ActivityLoginBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initViews()
    }

    private fun initViews() {
        supportFragmentManager.commit {
            supportFragmentManager.commit {
                add(R.id.fragment_container_view_login, LoginFragment())
                addToBackStack(null)
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (supportFragmentManager.backStackEntryCount == 0) finish()
    }
}