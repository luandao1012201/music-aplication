package com.example.musicapplication.ui.fragments

import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.example.musicapplication.R
import com.example.musicapplication.databinding.FragmentChangePasswordBinding
import com.example.musicapplication.ui.viewmodel.UserViewModel

class ChangePasswordFragment : Fragment() {
    private lateinit var binding: FragmentChangePasswordBinding
    private val userViewModel by viewModels<UserViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChangePasswordBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initListeners() {
        binding.btnConfirm.setOnClickListener {
            val oldPassword = binding.edtOldPassword.text.toString()
            val newPassword = binding.edtNewPassword.text.toString()
            if (validate(oldPassword, newPassword)) {
                userViewModel.changePassword(oldPassword, newPassword)
            }
        }
        binding.tvBack.setOnClickListener {
            parentFragmentManager.popBackStack()
        }
    }

    private fun initViews() {
        binding.tvErrorOldPassword.visibility = View.GONE
        binding.tvErrorNewPassword.visibility = View.GONE
        userViewModel.changePasswordResult.observe(this.viewLifecycleOwner) {
            if (it.first) {
                parentFragmentManager.popBackStack()
            }
            Toast.makeText(activity, it.second, Toast.LENGTH_SHORT).show()
        }
    }

    private fun validate(oldPassword: String, newPassword: String): Boolean {
        if (oldPassword.isEmpty()) {
            binding.edtOldPassword.requestFocus()
            binding.tvErrorOldPassword.visibility = View.VISIBLE
            binding.tvErrorOldPassword.text = "Không để trống"
            return false
        }
        if (oldPassword.length < 8 || oldPassword.length > 16) {
            binding.edtOldPassword.requestFocus()
            binding.tvErrorOldPassword.visibility = View.VISIBLE
            binding.tvErrorOldPassword.text = "Mật khẩu phải có từ 8 đến 16 ký tự"
            return false
        }
        if (oldPassword.contains(" ")) {
            binding.edtOldPassword.requestFocus()
            binding.tvErrorOldPassword.visibility = View.VISIBLE
            binding.tvErrorOldPassword.text = "Mật khẩu không được chứa khoảng trắng"
            return false
        }
        if (newPassword.isEmpty()) {
            binding.edtNewPassword.requestFocus()
            binding.tvErrorNewPassword.visibility = View.VISIBLE
            binding.tvErrorNewPassword.text = "Không để trống"
            return false
        }
        if (newPassword.length < 8 || newPassword.length > 16) {
            binding.edtNewPassword.requestFocus()
            binding.tvErrorNewPassword.visibility = View.VISIBLE
            binding.tvErrorNewPassword.text = "Mật khẩu phải có từ 8 đến 16 ký tự"
            return false
        }
        if (newPassword.contains(" ")) {
            binding.edtNewPassword.requestFocus()
            binding.tvErrorNewPassword.visibility = View.VISIBLE
            binding.tvErrorNewPassword.text = "Mật khẩu không được chứa khoảng trắng"
            return false
        }
        return true
    }
}