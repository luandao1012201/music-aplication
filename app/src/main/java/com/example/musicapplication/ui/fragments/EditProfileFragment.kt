package com.example.musicapplication.ui.fragments

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.example.musicapplication.R
import com.example.musicapplication.databinding.FragmentEditProfileBinding
import com.example.musicapplication.loadImage
import com.example.musicapplication.ui.viewmodel.UserViewModel

class EditProfileFragment : Fragment() {
    companion object {
        private const val IMAGE_REQUEST = 111
    }

    private lateinit var binding: FragmentEditProfileBinding
    private val userViewModel by viewModels<UserViewModel>()
    private val viewModel by activityViewModels<UserViewModel>()
    private var imageUri: Uri? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEditProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initViews() {
        userViewModel.getCurrentUser()
        userViewModel.currentUser.observe(this.viewLifecycleOwner) {
            it?.image?.let { image -> binding.ivAvatar.loadImage(image) }
            binding.edtName.setText(it?.username)
            binding.edtEmail.setText(it?.email)
        }
        userViewModel.updateUserResult.observe(this.viewLifecycleOwner) {
            if (it.first) {
                parentFragmentManager.popBackStack()
                viewModel.getCurrentUser()
            }
            Toast.makeText(activity, it.second, Toast.LENGTH_SHORT).show()
        }
    }

    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, IMAGE_REQUEST)
    }

    private fun initListeners() {
        binding.tvBack.setOnClickListener {
            parentFragmentManager.popBackStack()
        }
        binding.ivAvatar.setOnClickListener {
            selectImage()
        }
        binding.btnConfirm.setOnClickListener {
            val username = binding.edtName.text.toString()
            val user = userViewModel.currentUser.value
            if (validate(username)) {
                user?.username = username
                if (user != null) {
                    userViewModel.updateUser(user, imageUri)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK) {
            imageUri = data?.data
            try {
                binding.ivAvatar.loadImage(imageUri.toString())
            } catch (_: Exception) {
            }
        }
    }

    private fun validate(username: String): Boolean {
        if (username.isEmpty()) {
            binding.edtName.apply {
                error = "Không để trống"
                requestFocus()
            }
            return false
        }
        return true
    }
}