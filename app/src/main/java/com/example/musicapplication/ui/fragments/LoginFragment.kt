package com.example.musicapplication.ui.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.commit
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import com.example.musicapplication.R
import com.example.musicapplication.databinding.FragmentLoginBinding
import com.example.musicapplication.ui.activities.MainActivity
import com.example.musicapplication.ui.viewmodel.UserViewModel
import com.google.gson.Gson

class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding
    private val userViewModel by viewModels<UserViewModel>()
    private val sharedPreferences by lazy {
        activity?.getSharedPreferences(ProfileFragment.CURRENT_USER, Context.MODE_PRIVATE)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initViews() {
        binding.tvErrorPassword.visibility = View.GONE
        setFragmentResultListener(RegisterFragment.INFO_SIGNUP) { _, bundle ->
            val email = bundle.getString(RegisterFragment.EMAIL_SIGNUP).toString()
            val password = bundle.getString(RegisterFragment.PASSWORD_SIGNUP).toString()
            if (email.isNotEmpty() && password.isNotEmpty()) {
                binding.edtEmail.setText(email)
                binding.edtPassword.setText(password)
            }
        }
        userViewModel.authResult.observe(this.viewLifecycleOwner) {
            if (it.first) {
                userViewModel.getCurrentUser()
                activity?.startActivity(Intent(activity, MainActivity::class.java))
                activity?.finish()
            }
            Toast.makeText(activity?.applicationContext, it.second, Toast.LENGTH_SHORT).show()
        }
        userViewModel.currentUser.observe(this.viewLifecycleOwner) {
            sharedPreferences?.edit()?.putString(ProfileFragment.CURRENT_USER, Gson().toJson(it))
                ?.apply()
        }
    }

    private fun initListeners() {
        binding.tvToRegister.setOnClickListener {
            activity?.supportFragmentManager?.commit {
                add(R.id.fragment_container_view_login, RegisterFragment())
                addToBackStack(null)
            }
        }
        binding.btnLogin.setOnClickListener {
            val email = binding.edtEmail.text.toString().trim()
            val password = binding.edtPassword.text.toString().trim()
            if (validate(email, password)) {
                userViewModel.login(email, password)
            }
        }
    }

    private fun validate(email: String, password: String): Boolean {
        if (email.isEmpty()) {
            binding.edtEmail.apply {
                error = "Không để trống"
                requestFocus()
            }
            return false
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            binding.edtEmail.apply {
                error = "Định dạng email không chính xác"
                requestFocus()
            }
            return false
        }
        if (password.isEmpty()) {
            binding.edtPassword.requestFocus()
            binding.tvErrorPassword.visibility = View.VISIBLE
            binding.tvErrorPassword.text = "Không để trống"
            return false
        }
        if (password.length < 8 || password.length > 16) {
            binding.edtPassword.requestFocus()
            binding.tvErrorPassword.visibility = View.VISIBLE
            binding.tvErrorPassword.text = "Mật khẩu phải có từ 8 đến 16 ký tự"
            return false
        }
        if (password.contains(" ")) {
            binding.edtPassword.requestFocus()
            binding.tvErrorPassword.visibility = View.VISIBLE
            binding.tvErrorPassword.text = "Mật khẩu không được chứa khoảng trắng"
            return false
        }
        return true
    }
}