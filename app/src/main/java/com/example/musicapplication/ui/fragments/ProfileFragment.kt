package com.example.musicapplication.ui.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import com.example.musicapplication.R
import com.example.musicapplication.databinding.FragmentProfileBinding
import com.example.musicapplication.loadImage
import com.example.musicapplication.ui.activities.LoginActivity
import com.example.musicapplication.ui.viewmodel.Mp3ViewModel
import com.example.musicapplication.ui.viewmodel.UserViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.ktx.app
import com.google.gson.Gson

class ProfileFragment : Fragment() {
    companion object {
        const val CURRENT_USER = "CURRENT_USER"
    }

    private lateinit var binding: FragmentProfileBinding
    private val userViewModel by activityViewModels<UserViewModel>()
    private val mp3ViewModel by activityViewModels<Mp3ViewModel>()
    private val sharedPreferences by lazy {
        activity?.getSharedPreferences(CURRENT_USER, Context.MODE_PRIVATE)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initListeners()
    }

    override fun onResume() {
        super.onResume()
        userViewModel.getCurrentUser()
        mp3ViewModel.getMp3Charts()
    }

    private fun initView() {
        val currentUser = sharedPreferences?.getString(CURRENT_USER, "") ?: ""
        if (currentUser.isEmpty()) {
            binding.layoutNotLogin.visibility = View.VISIBLE
            binding.layoutLogin.visibility = View.GONE
        } else {
            binding.layoutNotLogin.visibility = View.GONE
            binding.layoutLogin.visibility = View.VISIBLE
        }
        userViewModel.getCurrentUser()
        userViewModel.currentUser.observe(this.viewLifecycleOwner) {
            if (it != null) {
                binding.layoutNotLogin.visibility = View.GONE
                binding.layoutLogin.visibility = View.VISIBLE
                it.image?.let { image -> binding.ivAvatar.loadImage(image) }
                binding.tvUsername.text = it.username
                binding.tvEmail.text = it.email
            }
        }
    }

    private fun initListeners() {
        binding.tvToLogin.setOnClickListener {
            activity?.startActivity(Intent(activity, LoginActivity::class.java))
        }
        binding.btnLogout.setOnClickListener {
            Firebase.auth.signOut()
            sharedPreferences?.edit()?.putString(CURRENT_USER, "")?.apply()
            binding.layoutNotLogin.visibility = View.VISIBLE
            binding.layoutLogin.visibility = View.GONE
            mp3ViewModel.getMp3Charts()
        }
        binding.btnEditProfile.setOnClickListener {
            parentFragmentManager.commit {
                add(R.id.fragment_container_view_tag, EditProfileFragment())
                addToBackStack(null)
            }
        }
        binding.btnChangePassword.setOnClickListener {
            parentFragmentManager.commit {
                add(R.id.fragment_container_view_tag, ChangePasswordFragment())
                addToBackStack(null)
            }
        }
    }
}