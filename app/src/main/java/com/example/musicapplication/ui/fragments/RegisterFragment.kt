package com.example.musicapplication.ui.fragments

import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import com.example.musicapplication.databinding.FragmentRegisterBinding
import com.example.musicapplication.ui.viewmodel.UserViewModel

class RegisterFragment : Fragment() {
    companion object {
        const val EMAIL_SIGNUP = "EMAIL_SIGNUP"
        const val PASSWORD_SIGNUP = "PASSWORD_SIGNUP"
        const val INFO_SIGNUP = "INFO_SIGNUP"
    }

    private lateinit var binding: FragmentRegisterBinding
    private val authViewMode by viewModels<UserViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initViews() {
        binding.tvErrorPassword.visibility = View.GONE
        authViewMode.authResult.observe(this.viewLifecycleOwner) {
            if (it.first) {
                val bundle = Bundle().apply {
                    putString(EMAIL_SIGNUP, binding.edtEmail.text.toString().trim())
                    putString(PASSWORD_SIGNUP, binding.edtPassword.text.toString().trim())
                }
                setFragmentResult(INFO_SIGNUP, bundle)
                parentFragmentManager.popBackStack()
            }
            Toast.makeText(activity?.applicationContext, it.second, Toast.LENGTH_SHORT).show()
        }
    }

    private fun initListeners() {
        binding.tvToLogin.setOnClickListener {
            parentFragmentManager.popBackStack()
        }
        binding.btnRegister.setOnClickListener {
            val name = binding.edtName.text.toString().trim()
            val email = binding.edtEmail.text.toString().trim()
            val password = binding.edtPassword.text.toString().trim()
            if (validate(name, email, password)) {
                authViewMode.register(name, email, password)
            }
        }
    }

    private fun validate(username: String, email: String, password: String): Boolean {
        if (username.isEmpty()) {
            binding.edtName.apply {
                error = "Không để trống"
                requestFocus()
            }
            return false
        }
        if (email.isEmpty()) {
            binding.edtEmail.apply {
                error = "Không để trống"
                requestFocus()
            }
            return false
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            binding.edtEmail.apply {
                error = "Định dạng email không chính xác"
                requestFocus()
            }
            return false
        }
        if (password.isEmpty()) {
            binding.edtPassword.requestFocus()
            binding.tvErrorPassword.visibility = View.VISIBLE
            binding.tvErrorPassword.text = "Không để trống"
            return false
        }
        if (password.length < 8 || password.length > 16) {
            binding.edtPassword.requestFocus()
            binding.tvErrorPassword.visibility = View.VISIBLE
            binding.tvErrorPassword.text = "Mật khẩu phải có từ 8 đến 16 ký tự"
            return false
        }
        if (password.contains(" ")) {
            binding.edtPassword.requestFocus()
            binding.tvErrorPassword.visibility = View.VISIBLE
            binding.tvErrorPassword.text = "Mật khẩu không được chứa khoảng trắng"
            return false
        }
        return true
    }
}