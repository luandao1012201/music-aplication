package com.example.musicapplication.ui.viewmodel

import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.musicapplication.model.User
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class UserViewModel : ViewModel() {
    private val auth = Firebase.auth
    private val store = Firebase.firestore
    private val storageRef: StorageReference = FirebaseStorage.getInstance().reference
    var authResult = MutableLiveData<Pair<Boolean, String>>()
    var currentUser = MutableLiveData<User?>()
    var updateUserResult = MutableLiveData<Pair<Boolean, String>>()
    var changePasswordResult = MutableLiveData<Pair<Boolean, String>>()

    fun register(name: String, email: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            var success: Boolean
            var message: String
            if (isEmailRegistered(email)) {
                success = false
                message = "Email đã được sử dụng"
            } else {
                try {
                    auth.createUserWithEmailAndPassword(email, password).await()
                    val user = auth.currentUser
                    user?.sendEmailVerification()?.await()
                    user?.let {
                        val userData = hashMapOf(
                            "username" to name,
                            "email" to email,
                            "image" to "https://firebasestorage.googleapis.com/v0/b/btl-hdv-c425d.appspot.com/o/icons8-user-100.png?alt=media&token=f0a6a5f5-56e1-4006-8332-7fa3e07efe44"
                        )
                        store.collection("users").document(user.uid).set(userData).await()
                    }
                    success = true
                    message = "Đăng ký thành công. Vui lòng kiểm tra email để xác nhận"
                } catch (e: Exception) {
                    success = false
                    message = "Đăng ký thất bại"
                }
            }
            withContext(Dispatchers.Main) {
                authResult.value = Pair(success, message)
            }
        }
    }

    fun login(email: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            var success: Boolean
            var message: String
            try {
                auth.signInWithEmailAndPassword(email, password).await()
                val verification = auth.currentUser?.isEmailVerified
                if (verification == true) {
                    success = true
                    message = "Đăng nhập thành công"
                } else {
                    success = false
                    message = "Kiểm tra email để xác thực"
                }
            } catch (e: Exception) {
                success = false
                message = "Đăng nhập thất bại"
            }
            withContext(Dispatchers.Main) {
                authResult.value = Pair(success, message)
            }
        }
    }

    private suspend fun isEmailRegistered(email: String): Boolean {
        val result = auth.fetchSignInMethodsForEmail(email).await()
        return !result.signInMethods.isNullOrEmpty()
    }

    fun getCurrentUser() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val user = store.collection("users")
                    .document(auth.currentUser?.uid.toString())
                    .get()
                    .await()
                    .toObject(User::class.java)
                withContext(Dispatchers.Main) {
                    currentUser.value = user
                }
            } catch (e: Exception) {
                Log.d("test123", e.message.toString())
            }
        }
    }

    fun updateUser(user: User, imageUri: Uri?) {
        viewModelScope.launch(Dispatchers.IO) {
            var success: Boolean
            var message: String
            try {
                if (imageUri != null) {
                    val image = uploadImage(imageUri)
                    user.image = image
                }
                val updates = hashMapOf(
                    "email" to user.email.toString(),
                    "username" to user.username.toString(),
                    "image" to user.image.toString(),
                )
                store.collection("users").document(auth.currentUser?.uid.toString())
                    .update(updates as Map<String, Any>).await()
                success = true
                message = "Cập nhật thông tin thành công"
            } catch (e: java.lang.Exception) {
                success = false
                message = "Cập nhật thông tin thất bại"
            }
            withContext(Dispatchers.Main) {
                updateUserResult.value = Pair(success, message)
            }
        }
    }

    private suspend fun uploadImage(imageUri: Uri): String {
        var url = ""
        try {
            val imagesRef: StorageReference = storageRef.child("${auth.currentUser?.uid}.jpg")
            imagesRef.putFile(imageUri).await()
            url = imagesRef.downloadUrl.await().toString()
        } catch (e: Exception) {
            Log.d("test123", e.message.toString())
        }
        return url
    }

    fun changePassword(oldPassword: String, newPassword: String) {
        viewModelScope.launch(Dispatchers.IO) {
            var success: Boolean
            var message: String
            val currentUser = auth.currentUser
            val credential = EmailAuthProvider.getCredential(currentUser?.email ?: "", oldPassword)
            try {
                currentUser?.reauthenticate(credential)?.await()
                currentUser?.updatePassword(newPassword)?.await()
                success = true
                message = "Đổi mật khẩu thành công"
            } catch (e: Exception) {
                success = false
                message = "Đổi mật khẩu thất bại"
            }
            withContext(Dispatchers.Main) {
                changePasswordResult.value = Pair(success, message)
            }
        }
    }
}